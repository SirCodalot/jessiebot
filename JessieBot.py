import math

def do_turn(pw):

    if len(pw.my_planets()) is 0:
        return

    conquest(pw)
    attack(pw)
    defense(pw)


def conquest(pw):
	for my_planet in pw.my_planets():
		nearest = get_nearest(pw, my_planet, pw.neutral_planets())

		if my_planet is nearest:
			continue

		future = get_future_rate(pw, nearest)

		if future <= 0 or my_planet.num_ships() < future + 1:
			continue

		pw.issue_order(my_planet, nearest, future + 1)


def defense(pw):
	for my_planet in pw.my_planets():
		attack = get_enemy_attack(pw, my_planet)

		if attack is 0:
			continue

		planets = pw.my_planets()
		planets.remove(my_planet)

		nearest = get_nearest(pw, my_planet, planets)

		if nearest is my_planet:
			continue

		if nearest.num_ships() < attack + 1:
			break

		pw.issue_order(nearest, my_planet, attack + 1)


def send_to_cheap_planets(pw):
	for planet in pw.neutral_planets():
		future_rate = get_future_rate(pw, planet)
		nearest = get_nearest_planet(pw, planet, future_rate)

		if nearest is planet:
			continue

		pw.issue_order(nearest, planet, future_rate)

def get_nearest_planet(pw, planet, min):
	nearest = planet
	nearest_distance = 1000

	for my_planet in pw.my_planets():
		if my_planet.num_ships() < min:
			continue

		dist = pw.distance(my_planet, planet)

		if dist is nearest_distance:
			if my_planet.num_ships() <= nearest.num_ships():
				continue
		elif dist < nearest:
			continue

		nearest = my_planet
		nearest_distance = dist

	return nearest


def get_nearest(pw, planet, others):
	nearest = planet
	nearest_dist = 10000

	for other in others:
		dist = pw.distance(other, planet)

		if dist > nearest_dist:
			continue

		nearest = other
		nearest_dist = dist

	return nearest

def get_future_rate(pw, planet):
	rate = planet.num_ships()

	for fleet in pw.fleets():
		rate = rate - fleet.num_ships()

	return rate


def get_enemy_attack(pw, planet):
	attack = 0

	for fleet in pw.enemy_fleets():
		if fleet.destination_planet() is not  planet:
			continue

		attack = attack + fleet.num_ships()

	return attack

def attack(pw):
	for my_planet in pw.my_planets():
		nearest = get_nearest(pw, my_planet, pw.enemy_planets())
		if nearest is my_planet:
			continue
		health = nearest.num_ships()
		if health < 0 or my_planet.num_ships() < health + 1:
			continue
		pw.issue_order(my_planet, nearest, health + 1)